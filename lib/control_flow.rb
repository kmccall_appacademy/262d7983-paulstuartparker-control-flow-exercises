# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  alpha = "abcdefghijklmnopqrstuvwxyz"
  str.delete(alpha)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[str.length/2]
  else
    return str[(str.length/2 - 1)..(str.length/2)]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count("aeiouAEIOU")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_s = ""
  arr.each_with_index do |el, idx|
    unless idx == arr.length - 1
    new_s << el + separator
    else
      new_s << el
    end
  end
  new_s
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_s = ""
  str.split("").each_with_index do |char, idx|
    if idx.even?
      new_s << char.downcase
    else
      new_s << char.upcase
    end
  end
  new_s

end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_s = []
  str.split.each do |word|
    if word.length >= 5
      new_s << word.reverse!
    else
      new_s << word
    end
  end
new_s.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  range = (1..n).to_a
  range.map do |int|
    if int % 3 == 0 && int % 5 == 0
      int = "fizzbuzz"
    elsif int % 3 == 0
      int = "fizz"
    elsif int % 5 == 0
      int = "buzz"
    else int
    end
  end
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  arr.each do |el|
    new_arr.unshift(el)
  end
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2...num).each do |i|
    return false if num % i == 0 || num == 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |i|
    factors << i if num % i == 0
  end
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select {|i| prime?(i)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = arr.select {|x| x.odd?}
  even = arr.select {|y| y.even?}
  if odd.length > even.length
    return even[0]
  else
    return odd[0]
  end
end
